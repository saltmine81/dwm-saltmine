# dwm-saltmine

My build of Suckless dwm, dynamic window manager.

Original source:

- [https://dwm.suckless.org/](https://dwm.suckless.org/)
- [https://dl.suckless.org/dwm/dwm-6.2.tar.gz](https://dl.suckless.org/dwm/dwm-6.2.tar.gz)

Patches applied:

- alpha [https://dwm.suckless.org/patches/alpha/](https://dwm.suckless.org/patches/alpha/)
- useless gap [https://dwm.suckless.org/patches/uselessgap/](https://dwm.suckless.org/patches/uselessgap/)
- attachaside [http://dwm.suckless.org/patches/attachaside/](http://dwm.suckless.org/patches/attachaside/)
- pertag [http://dwm.suckless.org/patches/pertag/](http://dwm.suckless.org/patches/pertag/)
